for filepp in $(find selinux_pp/ -name "*.pp")
do
echo "semodule -X 300 -i $filepp"
semodule -X 300 -i $filepp
done
setsebool -P httpd_run_stickshift 1
setsebool -P httpd_setrlimit 1
setsebool -P httpd_mod_auth_pam 1
setsebool -P httpd_setrlimit 1
setsebool -P httpd_can_network_connect 1
setsebool -P httpd_graceful_shutdown 1
setsebool -P httpd_can_network_relay 1
setsebool -P nis_enabled 1
setsebool -P httpd_tmp_exec 1
setsebool -P httpd_read_user_content 1
setsebool -P domain_kernel_load_modules 1

/sbin/restorecon -v /etc/sysconfig/ip6tables
/sbin/restorecon -v /etc/sysconfig/iptables
